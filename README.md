Flask microservices to be used in RealSpace

Steps to install:

1: Create virtualenv (optional)

```
#!bash

virtualenv realspace-flask
cd realspace-flask/
source bin/activate

```

2: Clone this repo

```
#!bash

git clone https://<username>@bitbucket.org/propspace/realspace-flask.git
cd realspace-flask/
```


3: install python dependencies

```
#!bash

pip install -r requirements.txt
```


4: install other dependencies (wkhtmltopdf)

```
#!bash

./pdfkit-before-script.sh
```


5: run migration to setup db (if required)

```
#!bash

python migration.py
```


6: start the flask service

```
#!bash

export FLASK_APP=flask_server.py
flask run --host=0.0.0.0
```