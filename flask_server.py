# -*- coding: utf-8 -*- 
import json
import requests
import os.path
import pdfkit

from flask import jsonify, request, send_from_directory, render_template
from zoopla_summary import get_summary , get_zoopla_summary_pdf_path
from lib.utils import removeNonAscii
from common import FLASK_HOST, FLASK_PORT, AUTH_URL, DEBUG , ZOOPLA_PDF_OPTIONS , WKHTMLTOPDF_PATH, app , db_session
import common

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route("/")
def hello():
    raise InvalidUsage('Route not implemented!')

def authenticate(_request):
	auth_headers = {'Content-Type': 'application/json'}
	auth_headers['User-Agent'] = _request.headers['User-Agent']
	auth_headers['X-Forwarded-For'] = _request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in _request.headers else _request.remote_addr
	v = requests.post(AUTH_URL, cookies = _request.cookies, headers = auth_headers)

	if v.status_code != 200:
		raise InvalidUsage('This user is not authorized', status_code=401)

	return v

@app.route("/zoopla/summary/<area>", methods=['GET', 'POST'])
def zoopla_summary(area):
	
	authenticate(request)
	response = get_summary(area)
	return jsonify(response)



@app.route('/zoopla/summarypdf/<area>', methods=['GET', 'POST'])
def zoopla_summary_pdf(area):

	user = authenticate(request)
	user = json.loads(user.content)
	summary = get_summary(area)
	fname = get_zoopla_summary_pdf_path(area)
	if os.path.isfile(fname) == False:
	
		html = render_template('area_info.html', summary=summary, area=area.upper(),
			currentUser={'currencySymbol': user['currencySymbol'].encode('utf-8')}, 
			valueChange=abs(summary['value_change']) if 'value_change' in summary else None,
			valueChangePositive=(summary['value_change'] > 0) if 'value_change' in summary else None)

		if DEBUG:
			print 'HTML = ', removeNonAscii(html)

		config = pdfkit.configuration()
		if WKHTMLTOPDF_PATH:
			config.wkhtmltopdf = WKHTMLTOPDF_PATH
		pdfkit.from_string(html, fname, options=ZOOPLA_PDF_OPTIONS, configuration = config)
		
	elif DEBUG: 
		print 'PDF for area {0} already generated'.format(area)

	pdfname = area.upper() + '.pdf'
	pdfdir = fname.replace(pdfname, '')
	return send_from_directory(pdfdir, pdfname)





@app.context_processor
def my_utility_processor():

    def format_number (num, precision=0):
    	return "{:,}".format(int(num) if precision == 0 else round(float(num), precision))

    return dict(format_number=format_number)

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()

if __name__ == "__main__":
	common.app.run(host=FLASK_HOST, port=FLASK_PORT)
