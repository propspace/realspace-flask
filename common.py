# -*- coding: utf-8 -*- 
import ConfigParser
import redis
from datetime import datetime , timedelta
# from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import sys

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

CONFIG_PATH = '/home/propspace/webapps/zoopla_service/realspace-flask/'
if len(sys.argv) > 1:
    CONFIG_PATH = str(sys.argv[1])

Config = ConfigParser.ConfigParser()

Config.read(CONFIG_PATH + 'flask_server.ini')
CACHE_LIFE = Config.get('Redis', 'CacheLife')
WKHTMLTOPDF_PATH = Config.get('PDFKit','wkhtmltopdf')
DEBUG = Config.get('General', 'Debug')


Config.read(CONFIG_PATH + 'local_settings.ini')
REDIS_IP = Config.get('Redis', 'Ip')
ZOOPLA_API_KEY = Config.get('Zoopla', 'ApiKey')
AUTH_URL = Config.get('General', 'AuthUrl')
FLASK_HOST = Config.get('General', 'Host')
FLASK_PORT = Config.get('General', 'Port')
ZOOPLA_PDF_FOOTER_TEMPLATE = Config.get('Paths','ZooplaPdfFooter')
ZOOPLA_PDF_CACHE = Config.get('Paths','ZooplaPdfCache')

redis = redis.StrictRedis(host=REDIS_IP)

app = Flask(__name__)

engine = create_engine(Config.get('MySQL', 'uri'), convert_unicode=True, 
    pool_recycle=Config.get('MySQL', 'ConnectionPoolRecycleTime'))

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
ModelBase = declarative_base()
ModelBase.query = db_session.query_property()

# app.config['SQLALCHEMY_DATABASE_URI'] = Config.get('MySQL', 'uri')
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# app.config['SQLALCHEMY_POOL_RECYCLE'] = 60

# db = SQLAlchemy(app)

ZOOPLA_PDF_OPTIONS = {
    'page-size': 'A4',
    'margin-top': '0.75in',
    'margin-right': '0.75in',
    'margin-bottom': '0.75in',
    'margin-left': '0.75in',
    'encoding': "UTF-8",
    'custom-header' : [
        ('Accept-Encoding', 'gzip')
    ],
    'no-outline': None,
    "footer-html": ZOOPLA_PDF_FOOTER_TEMPLATE
}