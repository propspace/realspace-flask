from common import ModelBase , CACHE_LIFE
from datetime import datetime , timedelta
from sqlalchemy import Column, Integer, String, Text, DateTime

class ZooplaSummary(ModelBase):
	__tablename__ = "zoopla_summary"
	id = Column(Integer, primary_key=True)
	area = Column(String(10), unique=True)
	summary = Column(Text)
	expires = Column(DateTime)

	def __init__(self, area, summary):
		self.area = area
		self.summary = summary
		self.expires = datetime.now() + timedelta(0, int(CACHE_LIFE))

	def __repr__(self):
		return '<Zoopla Summary for %r>' % self.area