from datetime import datetime , timedelta
import os.path
import grequests
import json
from common import  ZOOPLA_PDF_CACHE, CACHE_LIFE , ZOOPLA_API_KEY , DEBUG , redis , db_session
from models import ZooplaSummary

def get_zoopla_summary_pdf_path(area):
	return ZOOPLA_PDF_CACHE + '{0}.pdf'.format(area.upper())

def get_summary(area):		
	redisKey = 'zoopla-summary:' + area.upper()
	if(redis.exists(redisKey)):
		if(DEBUG):
			print 'redis hit : ' + redisKey
		return json.loads(redis.get(redisKey))
	else:
		if(DEBUG):
			print 'redis miss : ' + redisKey

		zs = ZooplaSummary.query.filter(ZooplaSummary.area == area.upper()).first()
		if zs and zs.expires > datetime.now():
			if DEBUG:
				print 'mysql hit : ' + area
			redis.set(redisKey, zs.summary)
			redis.expire(redisKey, CACHE_LIFE) 
			return json.loads(zs.summary)
		else:	
			if DEBUG:
				print 'mysql miss : ' + area

			fname = get_zoopla_summary_pdf_path(area)
			if(os.path.isfile(fname)):
				if DEBUG: 
					print 'removing ', fname
				os.remove(fname)

			url_base = 'http://api.zoopla.co.uk/api/v1'
			urls = [
				url_base + "/area_value_graphs.json?api_key={0}&area={1}".format(ZOOPLA_API_KEY, area),
				url_base + "/average_area_sold_price.json?api_key={0}&area={1}&output_type=outcode".format(ZOOPLA_API_KEY, area),
				url_base + "/zed_index.json?api_key={0}&area={1}".format(ZOOPLA_API_KEY, area),
				url_base + "/local_info_graphs.json?api_key={0}&area={1}".format(ZOOPLA_API_KEY, area)
			]
			rs = (grequests.get(u) for u in urls)
			area_value_graphs = {}
			average_area_sold_price = {}
			zed_index = {}
			local_info_graphs = {}
			for r in grequests.imap(rs):
				if 'area_value_graphs' in r.url:
					area_value_graphs = json.loads(r.content)

				if 'average_area_sold_price' in r.url:
					average_area_sold_price = json.loads(r.content)

				if 'zed_index' in r.url:
					zed_index = json.loads(r.content)

				if 'local_info_graphs' in r.url:
					local_info_graphs = json.loads(r.content)

			response = {}
			response['area_values_url'] = area_value_graphs['area_values_url']
			response['average_values_graph_url'] = area_value_graphs['average_values_graph_url']
			response['value_trend_graph_url'] = area_value_graphs['value_trend_graph_url']
			response['value_ranges_graph_url'] = area_value_graphs['value_ranges_graph_url']
			response['home_values_graph_url'] = area_value_graphs['home_values_graph_url']

			response['people_graph_url'] = local_info_graphs['people_graph_url']
			response['education_graph_url'] = local_info_graphs['education_graph_url']
			response['crime_graph_url'] = local_info_graphs['crime_graph_url']
			response['council_tax_graph_url'] = local_info_graphs['council_tax_graph_url']

			if average_area_sold_price['average_sold_price_1year']:
				response['average_price_paid'] = average_area_sold_price['average_sold_price_1year'].encode('ascii','ignore')
				if response['average_price_paid'].isdigit():
					response['average_price_paid'] = float(response['average_price_paid'])

			if average_area_sold_price['number_of_sales_1year']:
				response['sales'] = average_area_sold_price['number_of_sales_1year'].encode('ascii','ignore')
				if response['sales'].isdigit():
					response['sales'] = float(response['sales'])

			if zed_index['zed_index']:
				response['current_average_value'] = zed_index['zed_index'].encode('ascii','ignore')
				if response['current_average_value'].isdigit():
					response['current_average_value'] = float(response['current_average_value'])
					response['previous_average_value'] = float(zed_index['zed_index_1year'])
					if response['current_average_value'] != 0 and response['previous_average_value'] != 0:
						response['value_change'] = response['current_average_value'] - response['previous_average_value']
						response['value_change_pct'] = round(
							response['current_average_value'] * 100.0 / response['previous_average_value'] - 100.0 , 2)


			jsonified_response = json.dumps(response)
			redis.set(redisKey, jsonified_response)
			redis.expire(redisKey, CACHE_LIFE) 
			if zs:
				zs.summary = jsonified_response
				zs.expires = datetime.now() + timedelta(0, int(CACHE_LIFE))
			else:
				db_session.add(ZooplaSummary(area.upper(),jsonified_response))
			db_session.commit()
			return response